<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use LaravelFullCalendar\Facades\Calendar;

class EventController extends Controller
{
    public function index()
    {

        $events = Event::all();
        $event = [];

        foreach ($events as $raw) {
            $event[] = Calendar::event(
                $raw->title,
                false, // for timing
                new \DateTime($raw->start_date),
                new \DateTime($raw->end_date),
                $raw->id,
                [
                    'color' => $raw->color
                ]

            );
        }

        $calendar = Calendar::addEvents($event);


        return view('events.index', compact('calendar','events'));
    }

    public function create()
    {

        return view('events.create');

    }//end of create function

    public function store(Request $request)
    {

        $data = $request->validate([
            'title' => 'required',
            'color' => 'required',
            'start_date' => 'required|date|after_or_equal:' . Carbon::now()->toDateString(),
            'end_date' => 'required|date'
        ]);

        Event::create($data);

        return redirect()->route('events.index')->with('success', 'data stored successfully');
    }//end of store function

    public function edit($id){

        $event = Event::find($id);
        return view('events.edit',compact('event'));

    }//end of edit function

    public function update(Request $request,$id){

        $event =Event::find($id);
        $data = $request->validate([
            'title' => 'required',
            'color' => 'required',
            'start_date' => 'required|date|after_or_equal:' . Carbon::now()->toDateString(),
            'end_date' => 'required|date'
        ]);
        $event->update($data);
        return redirect()->route('events.index')->with('success', 'data stored successfully');

    }//end of update function

    public function destroy ($id){

        $event = Event::find($id);
        $event->delete();
        return redirect()->route('events.index')->with('success', 'data stored successfully');

    }//end of destroy  function
}
