<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
</head>
<body>

<div class="container" style="margin-top: 100px">

    @if($errors->any())

        @foreach($errors->all() as $error)

            <div class="alert alert-danger">{{$error}}</div>
        @endforeach
    @endif
    <div class="row">


            <form action="{{route('events.store')}}" class="form" method="post">
                @csrf
                <div class="form-group">
                    <label for="" class="form-label">Title</label>
                    <input type="text" class="form-control" name="title">
                </div>

                <div class="form-group">
                    <label for="" class="form-label">Color</label>
                    <input type="color" class="form-control" name="color">
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Start Date</label>
                    <input type="datetime-local" class="form-control" name="start_date">
                </div>
                <div class="form-group">
                    <label for="" class="form-label">End Date</label>
                    <input type="datetime-local" class="form-control" name="end_date">
                </div>

                <div class="form-group mt-2">
                    <button type="submit" class="btn btn-success form-control">Save</button>
                </div>
            </form>



    </div>
</div>

</body>
</html>
