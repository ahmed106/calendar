<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>

    {{--    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>--}}
    {{--    <script src="https://kit.fontawesome.com/2eb6657f57.js" crossorigin="anonymous"></script>--}}
    {{--    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>--}}

    <link rel="stylesheet" href="{{asset('plugins/calendar/main.min.css')}}">
    <script src="{{asset('plugins/calendar/main.min.js')}}"></script>

</head>
<body>

<h4 class="text-center mt-2 mb-2"> Full calendar</h4>


<div class="container">
    <div id='calendar'></div>
</div>

<div class="container" style="width: 800px ;height: 800px">
    <div id="dialog">
        <div id="dialog-body">
            <form method="post"  action="{{route('events.store')}}" id="dayClick">
                @csrf
                <div class="form-group">
                    <label for="" class="form-label">Title</label>
                    <input type="text" class="form-control" name="title">
                </div>

                <div class="form-group">
                    <label for="" class="form-label">Start Date</label>
                    <input type="text" class="form-control" name="start">
                </div>

                <div class="form-group">
                    <label for="" class="form-label">End Date</label>
                    <input type="text" class="form-control" name="end">
                </div>

                <div class="form-group">
                    <label class="form-check-inline" for="">All Day</label>
                    <input type="checkbox" value="1" class="form-check-input" name="allDay"> All Day
                    <input type="checkbox" value="0" class="form-check-input" name="allDay"> Partial
                </div>

                <div class="form-group">
                    <label for="" class="form-label">Color</label>
                    <input type="color" class="form-control" name="color">
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Color Text</label>
                    <input type="color" class="form-control" name="text_color">
                </div>
            </form>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        var calendar = $('#calendar').fullCalendar({
            height: 800,
            defaultView: 'month',
            editable: false,
            showNonCurrentDates: false,
            yearColumns: 3,
            selectable:true,

            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'year,month,basicWeek,basicDay'
            },
            dayClick:function (date,event,view){
                    alert(view)
            }


        })


    })
</script>


{{--<div class="container m-2">--}}
{{--    <a href="{{route('events.create')}}" class="btn btn-success">Create Event </a>--}}
{{--    <a href="" class="btn btn-primary">Edit Event </a>--}}
{{--    <a href="" class="btn btn-danger">Delete Event </a>--}}
{{--</div>--}}
{{--<div class="container" style="width: 1000px; height: 1000px">--}}

{{--    <div class="row">--}}
{{--        <div class="col-md-8">--}}
{{--            {!! $calendar->calendar() !!}--}}
{{--            {!! $calendar->script() !!}--}}
{{--        </div>--}}
{{--        <div class="col-md-4">--}}
{{--            <h5>Events</h5>--}}
{{--            <table class="table table-hover table-bordered text-center">--}}
{{--                <thead>--}}
{{--                <th>#</th>--}}
{{--                <th>Title</th>--}}
{{--                <th>Actions</th>--}}
{{--                </thead>--}}

{{--                @foreach($events as $index=> $event)--}}
{{--                    <tbody>--}}
{{--                    <tr>--}}
{{--                        <td>{{++$index}}</td>--}}
{{--                        <td>{{$event->title}}</td>--}}
{{--                        <td>--}}
{{--                            <a href="{{route('events.edit',$event->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-edit fa-sm"></i></a>--}}
{{--                            <form  method="post" action="{{route('events.destroy',$event->id)}}">--}}

{{--                                @csrf--}}
{{--                                @method('delete')--}}
{{--                              <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash fa-sm"></i></button>--}}
{{--                            </form>--}}

{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    </tbody>--}}
{{--                @endforeach--}}


{{--            </table>--}}
{{--        </div>--}}
{{--    </div>--}}


{{--</div>--}}


<script>

</script>
</body>
</html>
